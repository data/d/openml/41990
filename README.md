# OpenML dataset: GTSRB-HueHist

https://www.openml.org/d/41990

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

The German Traffic Sign Benchmark is a multi-class, single-image classification challenge held at the International Joint Conference on Neural Networks (IJCNN) 2011. We cordially invite researchers from relevant fields to participate: The competition is designed to allow for participation without special domain knowledge.

# Pre-calculated features

To allow scientists without a background in image processing to participate, we several provide pre-calculated feature sets. Each feature set contains the same directory structure as the training image set. For details on the parameters of the feature algorithm, please have a look at the file Feature_description.txt which is part of each archive file.
 
\# Hue Histograms
    
For each image in the training set, the file contains a 256-bin histogram of hue values (HSV color space).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41990) of an [OpenML dataset](https://www.openml.org/d/41990). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41990/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41990/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41990/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

